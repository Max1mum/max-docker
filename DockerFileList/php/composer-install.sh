#!/usr/bin/env bash

file="composer.json"
PARAM=${@:1}
if [ ! -f "$file" ]
then
	echo -e "\e[31m$file sa nenašiel\e[0m"
    exit 1
fi


if [[ -z "$1" ]]; then
    PARAM='artemis'
fi

composer install;

mkdir -p /tmp/$PARAM

#cp -r vendor/ /tmp/$PARAM
rsync -ah --info=progress2 vendor/ /tmp/$PARAM/vendor
ln -s /usr/share/nginx/html/artemis-project/src /tmp/$PARAM

