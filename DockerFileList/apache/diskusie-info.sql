-- Potrebne vykonat kvoli redirectovaniu:

UPDATE disc_theme SET url = substring(url, 1, (LENGTH(url) - 20)) WHERE LENGTH(url)>240 ORDER BY LENGTH(url) DESC; -- zmazat z vysledkov poslednych 20 znakov

UPDATE disc_theme SET url = REPLACE(REPLACE(url, '.sme.sk', '.sme.localhost'), 'https://', 'http://');
