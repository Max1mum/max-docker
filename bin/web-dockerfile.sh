#!/bin/bash

SCRIPTDIR="$(dirname "$0")"

echo "FROM php:7.2-fpm

ENV DEBIAN_FRONTEND noninteractive

RUN adduser --disabled-password --gecos \"\" $USER

# Install dependencies.
RUN apt-get update && apt-get install -y \\
    zlib1g-dev \\
    wget \\
    vim \\
    nano \\
    libpng-dev \\
    libicu-dev \\
    libssl-dev \\
    sendmail \\
    git \\
    apt-utils \\
    libpq-dev

# Install php modules.
RUN apt-get install -y libxml2-dev libmcrypt-dev memcached checkinstall build-essential automake libtidy-dev geoip-bin geoip-database libgeoip-dev libfontconfig mysql-client unzip libnss3 libgconf-2-4

RUN docker-php-ext-install pdo pdo_mysql pcntl zip mbstring intl mysqli xmlrpc tidy pdo_pgsql
RUN cd /tmp/ \
    && pecl install geoip-beta  \
    && bash -c \"echo 'extension=geoip.so' >> /usr/local/etc/php/conf.d/40-geoip.ini\" \
    && mkdir -p /usr/share/GeoIP \
    && cd /usr/share/GeoIP \
    && wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz \
    && gunzip GeoLiteCity.dat.gz \
    && cp GeoLiteCity.dat GeoIPCity.dat
RUN docker-php-ext-configure xmlrpc
RUN docker-php-ext-configure calendar --enable-calendar
RUN docker-php-ext-install ftp
RUN docker-php-ext-configure ftp --enable-ftp
RUN docker-php-ext-install calendar
RUN apt-get update && apt-get install -y \\
        libfreetype6-dev \\
        libjpeg62-turbo-dev \\
    && docker-php-ext-install -j\$(nproc) iconv \\
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \\
    && docker-php-ext-install -j\$(nproc) gd exif \\
    && docker-php-ext-install ftp \\
    && docker-php-ext-configure ftp
RUN apt-get install -y mcrypt
#if needed memcached
#RUN echo extension=memcached.so >> /usr/local/etc/php/conf.d/memcached.ini
#TODO xdebug
#TODO pecl install opcache \\

RUN apt-get -y install libmemcached11 libmemcachedutil2 libmemcached-dev \\
    && pecl install memcached redis \\
    && docker-php-ext-enable memcached redis

COPY php.ini /usr/local/etc/php/conf.d/local-fpm.ini
#COPY memcache.ini /usr/local/etc/php/conf.d/memcache.ini

#if needed custom soap.ini else RUN docker-php-ext-install soap
# COPY soap.ini /usr/local/etc/php/conf.d/soap.ini

RUN docker-php-ext-install soap
RUN docker-php-ext-install opcache

# Override with our fpm conf.
COPY php-fpm.conf /usr/local/etc/php-fpm.conf
COPY composer-update-4min.sh /bin/composer-update-4min

# Install composer.
RUN php -r \"copy('https://getcomposer.org/installer', 'composer-setup.php');\"
RUN php composer-setup.php
RUN php -r \"unlink('composer-setup.php');\"
RUN mv composer.phar /usr/local/bin/composer

#ENV SPHINX_VERSION=2.2.10-release RE2_VERSION=2015-11-01 SPHINX_INDEX_DIR=/var/idx/sphinx SPHINX_LOG_DIR=/var/log/sphinx SPHINX_LIB_DIR=/var/lib/sphinx SPHINX_RUN_DIR=/var/run/sphinx SPHINX_DIZ_DIR=/var/diz/sphinx

RUN apt-get update && apt-get install -y  libodbc1 libpq5
#RUN wget -nv http://sphinxsearch.com/files/sphinxsearch_2.2.11-release-1~jessie_amd64.deb
#RUN dpkg -i sphinxsearch_2.2.11-release-1~jessie_amd64.deb
#RUN apt-get install -f
#RUN rm -rf sphinxsearch_2.2.11-release-1~jessie_amd64.deb

RUN wget https://bitbucket.org/ariya/phantomjs/downloads/
COPY phantomjs-2.1.1-linux-x86_64.tar.bz2 /tmp/phantomjs-2.1.1-linux-x86_64.tar.bz2
RUN tar -xvjf /tmp/phantomjs-2.1.1-linux-x86_64.tar.bz2
RUN cp phantomjs-2.1.1-linux-x86_64/bin/phantomjs /bin/phantomjs
RUN rm -rf phantomjs-2.1.1-linux-x86_6*
RUN rm -rf /tmp/phantomjs-2.1.1-linux-x86_64.tar.bz2

RUN docker-php-ext-install gettext
RUN yes '' | pecl install -f mcrypt
RUN echo \"extension=mcrypt.so\" > /usr/local/etc/php/conf.d/mcrypt.ini
RUN cd /tmp && wget https://github.com/alexeyrybak/blitz/archive/0.10.4-PHP7.tar.gz && tar zxvf 0.10.4-PHP7.tar.gz && cd blitz-0.10.4-PHP7 \
 && phpize && ./configure && make install \
 && echo 'extension=blitz.so' > /usr/local/etc/php/conf.d/blitz.ini
#COPY hosts /etc/hosts
RUN echo 'alias ll=\"ls -al\"' >> /home/$USER/.bashrc

#VOLUME ["${SPHINX_INDEX_DIR}", "${SPHINX_LOG_DIR}", "${SPHINX_LIB_DIR}", "${SPHINX_RUN_DIR}", "${SPHINX_DIZ_DIR}"]

WORKDIR /usr/share/nginx/html

CMD [\"php-fpm\", \"--allow-to-run-as-root\"]\
" > $SCRIPTDIR/../images/dev/web/Dockerfile