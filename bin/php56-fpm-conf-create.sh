#!/bin/bash

SCRIPTDIR="$(dirname "$0")"

echo "; From https://github.com/docker-library/php/blob/ae130b2f845162fbf84da0ffad07d7a64eff57cd/5.6/fpm/php-fpm.conf
; This file was initially adapated from the output of: (on PHP 5.6)
;   grep -vE '^;|^ *$' /usr/local/etc/php-fpm.conf.default

[global]

error_log = /var/log/php/error.log
log_level = warning
daemonize = no

[www]

clear_env = no
catch_workers_output = yes

; if we send this to /proc/self/fd/1, it never appears
access.log = /var/log/php/access.log

chdir = /usr/share/nginx/html
user = "$USER"
group = "`id -g -n`"

listen = [::]:9999
request_terminate_timeout = 600

pm = dynamic
pm.max_children = 4000
pm.start_servers = 20
pm.min_spare_servers = 10
pm.max_spare_servers = 30
pm.max_requests = 1000

php_flag[display_errors] = off
php_flag[display_startup_errors] = off
php_admin_value[display_errors] = off
php_admin_value[error_log] = /var/log/php/error.log
php_admin_flag[log_errors] = on\
" > $SCRIPTDIR/../DockerFileList/php56/php-fpm.conf